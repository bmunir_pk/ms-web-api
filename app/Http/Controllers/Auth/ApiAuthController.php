<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Patient;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;
use PeterPetrus\Auth\PassportToken;

class ApiAuthController extends Controller
{
    public function login (Request $request) {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if (!auth()->attempt($loginData)) {
            return response(['message' => 'Invalid Credentials']);
        }
        $user = User::where('email', $request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                //Passport::tokensExpireIn(Carbon::now()->addMinute(3));
               // Passport::refreshTokensExpireIn(Carbon::now()->addMinute(3));
                $token = auth()->user()->createToken('authToken');
                //$token = $user->createToken('Laravel Password Grant Client');
                $expiration = $token->token->expires_at->diffInSeconds(Carbon::now());
                $token->token->expires_at = Carbon::now()->addMinute(env('TOKEN_EXPIRY_GAP',(24*60)));
                $token->token->save();
                $response = ['token' => $token->accessToken, 'user' => auth()->user(), 'expires_at' => $expiration ];
                return response($response, 200);
            } else {
                $response = ["message" => "Unauthorized user"];
                return response($response, 401);
            }
        } else {
            $response = ["message" =>'Unauthorized'];
            return response($response, 401);
        }
    }

    public function logout (Request $request) {
        $token = $request->user()->token();
        $token->revoke();
        $response = ['message' => 'You have been successfully logged out!'];
        return response($response, 200);
    }

    public function register (Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'type' => 'integer',
        ]);
        if ($validator->fails())
        {
            return response(['data'=>$validator->errors()], 422);
        }
        $request['password']=Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);
        $request['type'] = $request['type'] ? $request['type']  : 0;
        $request['status'] = 'Inactive';
        $request['verification_code'] = substr(Hash::make($request['password']),0,15);
        $request['is_doctor'] = $request['is_doctor'];
        $user = User::create($request->toArray());
        if($user) {
            //send email here
            Mail::send('emails.signup', ['request' => $request], function ($m) use ($user) {
                $m->from('no-reply@app.com', 'Welcome to Covid Detection App');
                $m->to($user->email, "Welcome to Covid Detection App")->subject('Verify your email address');
            });
            //$token = $user->createToken('Laravel Password Grant Client')->accessToken;
            $response = ['success'=> true, 'message' => ['title'=> 'success!', 'content' => 'We have sent you an email, please verify your account.']];
            return response($response, 200);
        }
        $response = ['success'=> false, 'message' =>['title' => 'Error', 'content' => 'Something Went wrong please try again!'] ];
        return response($response, 503);

    }

    public function verifyEmail(Request $request, $email, $code) {
        $email = base64_decode($email);
        $user = User::where('email', $email)->where('verification_code', $code)->first();
        if($user) {
            $user->status = 'Active';
            $user->verification_code = '';
            $user->save();
            if($user->is_doctor == 'no') {
                $patient = new Patient();
                $patient->fill(['user_id' => $user->id, 'name' => $user->name])->save();
            }
            return response()->json(['message'=> 'Thank you!!'], 200);
        }
        return response()->json(['message'=> 'Email not verified'], 422);
    }

    public function verifyToken(Request $request) {

        $user = User::where('email', $request->get('email'))->firstOrFail();
        if ($user) {
            $decoded_token = PassportToken::dirtyDecode(
                $request->get('token')
            );
            $checkToken = \DB::table('oauth_access_tokens')->where([
                ['id', '=', $decoded_token["token_id"]],
                ['expires_at', '>', Carbon::now()]
            ])->first();

            if ( !$checkToken ) {
                return response()->json(['message'=> 'your token has expired, Please Login Again!', 'error' => true ], 422);
            }
            return response()->json(['message'=> 'ok'], 200);
        }
        return response()->json(['message'=> 'token has expired', 'error' => true], 422);
    }
}
