<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CompaniesController extends Controller
{
    public function index()
    {
        $response = ['message' => 'article index'];
        return response($response, 200);
    }

    public function updateProfile(Request $request) {
        dd(Auth::user());
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,id,',
        ]);
        if ($validator->fails())
        {
            return response(['data'=>$validator->errors()], 422);
        }
        $request['password']=Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);
        $request['type'] = $request['type'] ? $request['type']  : 0;
        $request['status'] = 'Inactive';
        $request['verification_code'] = substr(Hash::make($request['password']),0,15);
        $request['is_doctor'] = $request['is_doctor'];
        $user = User::create($request->toArray());
        if($user) {}
        $response = ['message' => 'profile updated'];
        return response($response, 200);
    }

    public function updatePassword(Request $request) {
        dd(Auth::user());
        $response = ['message' => 'password updated'];
        return response($response, 200);
    }
}
