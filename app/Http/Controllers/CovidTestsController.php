<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TestHistory;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CovidTestsController extends Controller
{

    public function index(Request $request)
    {

        $image = $request->input('file'); // image base64 encoded
        preg_match("/data:image\/(.*?);/",$image,$image_extension); // extract the image extension
        $image = preg_replace('/data:image\/(.*?);base64,/','',$image); // remove the type part
        $image = str_replace(' ', '+', $image);
        $imageName = 'image_' . time() . '.' . $image_extension[1]; //g

        \File::put(storage_path(). '/uploads/' . $imageName, base64_decode($image));

        $result = $this->getResult(str_replace('index.php/', '', url('/uploads/' . $imageName)));
        return response([
            "image_file" => str_replace('index.php/', '', url('/uploads/' . $imageName)),
            'result' => ($result == 0 ? 'Positive' : 'Negative')
        ], 200);
    }

    public function save(Request $request) {
        $obj = new TestHistory();
        $formData = $request->all();
        $formData['user_id'] = Auth::user()->id;
        $obj->fill($formData);
        if($obj->save()) {
            $response = ['message' => 'Test result has been saved'];
            return response($response, 200);
        }
        $response = ['message' => 'Something wrong with input, Please Try again!'];
        return response($response, 500);
    }

    private function getResult($file) {
        //$endpoint = "https://b49blhuyha.execute-api.us-east-1.amazonaws.com/prod/my-resource?myParam=".urlencode($file);
        $endpoint = env('MODEL_API','http://127.0.0.1:81/predict');
        $endpoint .= '?file_url='.urlencode($file);

        $ch = curl_init($endpoint);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL,$endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec ($ch);
        $info = curl_getinfo($ch);
        $http_result = $info ['http_code'];
        curl_close ($ch);
        return $output;
        //return ['status' => $statusCode, 'content' => $content];
    }
}
