<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Drug;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class DrugCompaniesController extends Controller
{
    public function index(Request $request)
    {
        $data = Company::all();
        $response = ['companies' => $data,'message' => ''];
        return response($response, 200);
    }

    public function save (Request $request) {
        $formData = $request->all();
        if(!empty($formData['logo']) && strpos("_______".$formData['logo'], 'http://') === false) {
            $formData['logo'] = $this->uploadFile($formData['logo']);
        }

        $validator = Validator::make($formData, [
            'title' => 'required',
            'description' => 'required',
            'address' => 'required',
            'phone_number' => 'required|digits:11',
            'logo' => 'required',
            'website_url' => 'required|regex:/^(http?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'
        ]);

        if($validator->fails()){
            return response()->json(['errors' => $validator->errors()], 503);
        }
        if(empty($formData['id'])) {
            $obj = new Company();
        } else {
            $obj = Company::find($formData['id']);
        }
        $obj->fill($formData);
        $obj->save();
        $response = ['company' => Company::where('id',$obj->id)->first(),'message' => ''];
        return response($response, 200);
    }

    public function delete (Request $request, $companyId) {
        $obj = Company::find($companyId);
        if($obj) {
            $obj->delete();
            return response(['message' => ['content' => "Record has been deleted", 'title', 'Success!']], 200);
        }
        return response(['message' => ['content' => "Record does not exists", 'title', 'Error!']], 503);
    }

    public function uploadFile ($image) {

        preg_match("/data:image\/(.*?);/",$image,$image_extension); // extract the image extension
        $image = preg_replace('/data:image\/(.*?);base64,/','',$image); // remove the type part
        $image = str_replace(' ', '+', $image);
        $imageName = 'image_' . time() . '.' . $image_extension[1]; //g

        \File::put(storage_path(). '/uploads/' . $imageName, base64_decode($image));

        return str_replace('index.php/', '', url('/uploads/' . $imageName));
    }

    public function search (Request $request) {
        return response()->json([
            'drugs' => Drug::where('title', 'like', '%'.$request->get('term').'%')
                ->get(),
            'success' => true
        ], 200);
    }
}
