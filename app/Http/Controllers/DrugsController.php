<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Drug;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class DrugsController extends Controller
{
    public function index()
    {
        $data = Drug::with('company')->get();
        $response = ['drugs' => $data,'message' => ''];
        return response($response, 200);
    }

    public function save (Request $request) {
        $formData = $request->all();

        $validator = Validator::make($formData, [
            'title' => 'required',
            'description' => 'required',
            'drug_company_id' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(['errors' => $validator->errors()], 503);
        }
        if(empty($formData['id'])) {
            $obj = new Drug();
        } else {
            $obj = Drug::find($formData['id']);
        }
        $obj->fill($formData);
        $obj->save();
        $response = ['drug' => Drug::where('id',$obj->id)->with('company')->first(),'message' => ''];
        return response($response, 200);
    }

    public function delete (Request $request, $drugId) {
        $obj = Drug::find($drugId);
        if($obj) {
            $obj->delete();
            return response(['message' => ['content' => "Record has been deleted", 'title', 'Success!']], 200);
        }
        return response(['message' => ['content' => "Record does not exists", 'title', 'Error!']], 503);
    }

    public function search (Request $request) {
        return response()->json([
            'drugs' => Drug::where('title', 'like', '%'.$request->get('term').'%')
                ->get(),
            'success' => true
        ], 200);
    }
}
