<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TestHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HistoriesController extends Controller
{
    public function covidTests()
    {
        if(Auth::user()->type == 1) {
            $data = TestHistory::with('patient')->get();
        } else {
            $data = TestHistory::where('user_id', Auth::user()->id)->with('patient')->get();
        }
        $response = ['data' => $data];
        return response($response, 200);
    }

    public function medicationHistory()
    {
        if(Auth::user()->is_doctor == 'no') {
            return response()->json(['patients' => [], 'message'=> ['title'=> 'Forbidden', 'message' => 'can not access this area'], 'success' => false], 503);
        }
        $response = ['message' => 'article index'];
        return response($response, 200);
    }

    public function getPatientTests(Request $request, $patient_id)
    {
        if(Auth::user()->type == 1) {
            $response = ['data' => TestHistory::where('patient_id', $patient_id)->with('patient')->get()];
        } else {
            $response = ['data' => TestHistory::where('patient_id', $patient_id)->where('user_id', Auth::user()->id)->with('patient')->get()];
        }
        return response($response, 200);
    }

    public function getMedicationHistory()
    {
        $response = ['message' => 'article index'];
        return response($response, 200);
    }
}
