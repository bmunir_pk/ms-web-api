<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Medication;
use App\Models\Patient;
use App\Models\PatientMedicationDetail;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PatientsController extends Controller
{
    public function index()
    {
        if(Auth::user()->is_doctor == 'yes' && Auth::user()->type == 0) {
            return response()->json(['patients' => Patient::where('user_id', Auth::user()->id)->orderBy('name', 'ASC')->get(), 'success' => true], 200);
        } else {
            if(Auth::user()->type == 1) {
                return response()->json(['patients' => Patient::orderBy('name', 'ASC')->get(), 'success' => true], 200);
            }
            return response()->json(['patients' => [], 'message'=> ['title'=> 'Forbidden', 'message' => 'can not access this area'], 'success' => false], 503);
        }
    }

    public function search(Request $request)
    {
        if(Auth::user()->is_doctor == 'no') {
            return response()->json(['patients' => [], 'message'=> ['title'=> 'Forbidden', 'message' => 'can not access this area'], 'success' => false], 503);
        }
        return response()->json([
            'patients' => Patient::where('user_id', Auth::user()->id)
                                    ->where('name', 'like', '%'.$request->get('term').'%')
                                    ->get(),
            'success' => true
        ], 200);
    }

    public function getPatient (Request $request, $patient) {

        if(Auth::user()->type != 1 && Auth::user()->is_doctor == 'no') {
            return response()->json(['patients' => [], 'message'=> ['title'=> 'Forbidden', 'message' => 'can not access this area'], 'success' => false], 503);
        }
        $patient = Patient::where('id', $patient)->with([
            'medications' => function( $q){
                return $q->orderBy('id', 'DESC')
                    ->with('medication_details', 'medication_details.drug');
        }])->first();
        return response()->json(['patient' => $patient, 'success' => true ], 200);
    }


    public function getPatientUser (Request $request, $user) {
        if(Auth::user()->is_doctor == 'yes') {
            return response()->json(['patients' => [], 'message'=> ['title'=> 'Forbidden', 'message' => 'can not access this area'], 'success' => false], 503);
        }
        $patient = Patient::where('user_id', $user)->with([
            'medications' => function( $q){
                return $q->orderBy('id', 'DESC')
                    ->with('medication_details', 'medication_details.drug');
            }])->first();
        return response()->json(['patient' => $patient, 'success' => true ], 200);
    }

    public function savePatient (Request $request) {
        if(Auth::user()->is_doctor == 'no') {
            return response()->json(['patients' => [], 'message'=> ['title'=> 'Forbidden', 'message' => 'can not access this area'], 'success' => false], 503);
        }
        //validate Patient Form
        if(!$request->get('id')) {
            $obj = new Patient();
        } else {
            $obj= Patient::find($request->get('id'));
            if(!$obj) {
                return response()->json(['message' => ['title'=>'Error!', 'content'=>'Patient not found'], 'success' => true ], 422);
            }
        }
        $request_data = $request->All();
        $validator = $obj->validateFormData($request_data);
        if($validator->fails())
        {
            return response()->json(['data' => $validator->errors(),
                'message' => [
                    'title' => 'Error!',
                    'content' => 'Input validation error'
                ]
                ], 503);
        }
        else {
            $obj->fill($request_data);
            $obj->user_id = Auth::user()->id;
            $obj->save();
            return response()->json(['message' => ['title'=> 'Success!','content' =>'Patient has been saved'], 'success' => true], 200);
        }
    }
    public function removePatient(Request $request, $patient) {
        if(Auth::user()->is_doctor == 'no') {
            return response()->json(['patients' => [], 'message'=> ['title'=> 'Forbidden', 'message' => 'can not access this area'], 'success' => false], 503);
        }
        $patientObj = Patient::find($patient);
        if(!$patientObj) {
            return response()->json(['message' => ['title' => 'Error!', 'content' => 'patient not found'], 'success' => true ], 422);
        }
        $patientObj->delete();

        return response()->json(['message' =>['title' => 'Success', 'content' => 'Patient has been removed'], 'success' => true ], 200);
    }

    public function medication()
    {
        if(Auth::user()->is_doctor == 'no') {
            return response()->json(['patients' => [], 'message'=> ['title'=> 'Forbidden', 'message' => 'can not access this area'], 'success' => false], 503);
        }
        $response = ['message' => ['content'=>'Patient Medication', 'title'=> 'Success!']];
        return response($response, 200);
    }

    public function saveMedication(Request $request, $patient_id)
    {
        $formData = $request->all();
        $formData['patient_id'] = $patient_id;
        $formData['user_id'] = Auth::user()->id;

        if(empty($formData['id'])) {
            $obj = new Medication();
            Medication::where('patient_id', $formData['patient_id'])->update(['status'=> 'previous']);
            $formData['status'] = 'current';
        } else {
            $obj = Medication::find($formData['id']);
        }
        $obj->fill($formData);
        if(!$obj->save()) {
            return response()->json(['data' => [],'message' =>['title' => 'Error!', 'content' => 'something went wrong, Try Again!']], 503);
        }
        if (!empty($formData['medicines'])) {
            foreach($formData['medicines'] as $medicine) {
                if(empty($medicine['id'])) {
                    $medObj = new PatientMedicationDetail();
                } else {
                    $medObj = PatientMedicationDetail::find($medicine['id']);
                }
                $medObj->medication_id = $obj->id;
                $medObj->drug_id = $medicine['drug']['id'];
                $medObj->drug_dosage = $medicine['drug_dosage'];
                $medObj->in_morning = (int)$medicine['in_morning'];
                $medObj->in_noon =  (int)$medicine['in_noon'];
                $medObj->in_evening =  (int)$medicine['in_evening'];
                $medObj->in_night =  (int)$medicine['in_night'];
                $medObj->remarks = $medicine['remarks'];
                $medObj->save();
            }
        }
        //dd($obj->with('medication_details', 'medication_details.drug')->first());
        $data = Medication::where('id', $obj->id)->with('medication_details', 'medication_details.drug')->first();
        return response()->json(['data' => $data,'message' =>['title' => 'Success', 'content' => 'Medication saved sucessfully!']], 200);
    }

    public function getMedicationDetail(Request $request, $patient_id, $medication_id) {
    }


}
