<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    public function index()
    {
        $response = ['message' => 'article index'];
        return response($response, 200);
    }

    public function updateProfile(Request $request) {
        $user = User::where('email', $request->get('email'))->first();
        if(!$user) {
            return response()->json(['message' => 'User Not Found'], 503);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,id,'.$user->id,
        ]);
        if ($validator->fails())
        {
            return response(['data'=>$validator->errors()], 422);
        }
        //$request['password']=Hash::make($request['password']);
        $user->update([
            'name' => $request->get('name'),
            'is_doctor' => $request->get('is_doctor')]);

        $response = ['message' => 'profile updated'];
        return response($response, 200);
    }

    public function updatePassword(Request $request) {
        $user = User::findOrFail(Auth::user()->id);

        /*
        * Validate all input fields
        */
        $validator = Validator::make($request->all(), [
            'oldPassword' => 'required:password:api',
            'newPassword' => 'required|confirmed|different:oldPassword'
        ]);
        if($validator->fails()) {
            return response()->json( ['data' => $validator->errors()], 503);
        }
        $user->password = Hash::make($request->get('newPassword'));
        $user->save();

        $response = ['message' => 'password has been updated'];
        return response($response, 200);
    }

    public function getAllUsers(Request $request) {
        $user = User::where('type', 0) ->get();
        return response()->json( ['data' => $user], 200);
    }

    public function banUser(Request $request, $id) {
        $user = User::find($id);
        $user->status = 'Inactive';
        $user->save();
        return response()->json( ['user' => $user], 200);
    }

    public function unbanUser(Request $request, $id) {
        $user = User::find($id);
        $user->status = 'Active';
        $user->save();
        return response()->json( ['user' => $user], 200);
    }
}
