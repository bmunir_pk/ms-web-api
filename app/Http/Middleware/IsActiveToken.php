<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use PeterPetrus\Auth\PassportToken;

class IsActiveToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $decoded_token = PassportToken::dirtyDecode(
            $request->header('Authorization')
        );
        $checkToken = \DB::table('oauth_access_tokens')->where([
            ['id', '=', $decoded_token['token_id']],
            ['user_id', '=', $user->id],
            ['expires_at', '>', Carbon::now()]
        ])->first();

        if ( !$checkToken ) {
            \DB::table('oauth_access_tokens')->where([
                ['id', '=', $decoded_token['token_id']],
                ['expires_at', '>', Carbon::now()]
            ])->delete();
            return response()->json([
                'error'=>true,
                'message'=> 'Token time has expired. Please log in again.'
            ]);
        } else {
            \DB::table('oauth_access_tokens')->where([
                ['id', '=', $decoded_token['token_id']],
                ['expires_at', '>', Carbon::now()]
            ])->update(['expires_at' => Carbon::now()->addMinute(env('TOKEN_EXPIRY_GAP', (24*60)))]);
        }

        return $next($request);
    }
}
