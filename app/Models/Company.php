<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $table='drug_companies';

    protected $fillable = [
        'title',
        'description',
        'logo',
        'website_url',
        'phone_number',
        'address'
    ];
}
