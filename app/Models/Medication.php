<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medication extends Model
{
    use HasFactory;
    protected $fillable = ['patient_id', 'user_id', 'remarks', 'status'];
    public function medication_details () {
        return $this->hasMany(PatientMedicationDetail::class);
    }
}
