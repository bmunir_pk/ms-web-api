<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'age', 'address', 'phone', 'landline', 'user_id'];

    public function medications (){
        return $this->hasMany(Medication::class);
    }

    public function test_history() {
        return $this->hasMany(TestHistory::class);
    }

    public function validateFormData ($data) {
        $validator = \Validator::make($data, [
            'name' => 'required|max:100',
            'age' => 'required|numeric|max:125',
            'address' => 'required|max:150',
            'phone' => 'required|regex:/(0)[0-9]/|not_regex:/[a-z]/|min:9',
            'landline' => 'required|regex:/(0)[0-9]/|not_regex:/[a-z]/|min:9',
        ]);

        return $validator;
    }
}
