<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestHistory extends Model
{
    protected $table = 'test_history';
    use HasFactory;
    public $fillable = [ 'patient_id', 'user_id', 'image_path', 'test_result', 'remarks'];

    public function patient () {
        return $this->belongsTo(Patient::class);
    }
}
