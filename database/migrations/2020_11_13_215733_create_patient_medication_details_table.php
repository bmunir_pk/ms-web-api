<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientMedicationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_medication_details', function (Blueprint $table) {
            $table->id();
            $table->integer('medication_id');
            $table->integer('drug_id');
            $table->string('drug_dosage', 2);
            $table->boolean('in_morning')->default(0);
            $table->boolean('in_noon')->default(0);
            $table->boolean('in_evening')->default(0);
            $table->boolean('in_night')->default(0);
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_medication_details');
    }
}
