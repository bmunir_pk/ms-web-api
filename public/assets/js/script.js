
var $ = jQuery;

$(function () {
  $('[data-toggle="tooltip"]').tooltip();

  // $( ".sortable" ).sortable();
  // $( ".sortable" ).disableSelection();


  // $(".sortable").sortable({});
  // $( ".sortable" ).draggable({ revert: true  });

  // $(".sortable").sortable({
  //   connectWith: ".sortable",
  //   handle: ".dragable",
  //   cancel: ".portlet-toggle",
  //   placeholder: "portlet-placeholder ui-corner-all"
  // });
  //
  //
  // $(".sortable-dropdown").sortable({
  //   connectWith: ".sortable-dropdown",
  //   handle: ".dragable-secondary",
  //   cancel: ".portlet-toggle"
  // });

});

$(document).ready( function (){

  // var headerHeight = $(".header").outerHeight();
  // $(window).scroll(function() {
  //   var scroll = $(window).scrollTop();
  //     if (scroll >= 2) {
  //         $(".header").addClass("header-fixed");
  //     }
  //     else{
  //       $(".header").removeClass("header-fixed");
  //     }
  // });


  // Menu
  $("body").on('click', '.jsMenuLink', function (){
    $(".navwrap").addClass('active');
    return false;
  });
  $("body").on('click', ".jsNavIcon", function (){
    $(".navwrap").removeClass('active');
    $(".navwrap").removeClass('navclosed');
    return false;
  });
  $("body").on('click', ".hasdropdown > a", function (){
    if( $(this).parent().hasClass('active')){
      $(this).parent().removeClass('active');
    }
    else{
      $(this).parent().addClass('active');
    }
    return false;
  });
  $("body").on('click', ".jsNavClose", function (){
    $(".navwrap > ul > li").removeClass('active');
    $(".navwrap").addClass('navclosed');
    return false;
  });

  $("body").on('click', ".jsMenuLinkSecondary", function (){
    $(".navwrap").addClass('active-secondary');
    return false;
  });
  $("body").on('click', ".jsNavCloseSecondary",function (){
    $(".navwrap").removeClass('active-secondary');
    return false;
  });



  $(".js-keyedit").click(function (){
    if( $(this).parents('.dropdown').hasClass('dropdown-editing')){
      $(this).parents('.dropdown').removeClass('dropdown-editing');
    }
    else{
      $(this).parents('.dropdown').addClass('dropdown-editing');
    }
    return false;
  });

  // Add more products
  $(".js-productlink").click(function (){
    if( $(this).parents('li').hasClass('add-product-active')){
      $(this).parents('li').removeClass('add-product-active');
      $(this).parents('.product-wrap').removeClass('dropdownopen');
    }
    else{
      $(".product-nav > ul > li").removeClass('add-product-active')
      $(this).parents('li').addClass('add-product-active');
      $(this).parents('.product-wrap').addClass('dropdownopen');
    }
    return false;
  });

  $(".cancel-productpopup").click(function (){
    $(".product-nav > ul > li").removeClass('add-product-active');
    $('.product-wrap').removeClass('dropdownopen');
    return false;
  });


  // Login modal input field
  $("#emailField").keypress(function() {
      if($(this).val().length > 1) {
        $("#continue").show();
      } else {
        $("#continue").hide();
      }
  });
  $("#continueBtn").click(function (){
    $("#shortOptions").hide();
    $("#moreoptions").show();
    return false;
  });



});
