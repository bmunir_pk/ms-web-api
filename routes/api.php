<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['cors', 'json.response']], function () {
    // public routes
    Route::post('/login', [\App\Http\Controllers\Auth\ApiAuthController::class, 'login'])->name('login.api');
    Route::post('/register',[\App\Http\Controllers\Auth\ApiAuthController::class, 'register'])->name('register.api');
    Route::get('/verify-email/{email}/{code}',[\App\Http\Controllers\Auth\ApiAuthController::class, 'verifyEmail'])->name('register.verification');
    Route::post('/logout', 'Auth\ApiAuthController@logout')->name('logout.api');
    Route::post('/verify-token',[\App\Http\Controllers\Auth\ApiAuthController::class, 'verifyToken']);
    //Route::get("/verify/{email}/{code}", [\App\Http\Controllers\Auth\ApiAuthController::class, 'verifyEmail']);
    /*Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });*/
    Route::group(['middleware' => ['auth:api', 'is_active_token']], function () {
        Route::post('/update-password', [\App\Http\Controllers\UsersController::class, 'updatePassword'])->name('passwordupdate.api');
        Route::post('/update-profile', [\App\Http\Controllers\UsersController::class, 'updateProfile'])->name('profileupdate.api');
        //Route::get('/update-profile', [\App\Http\Controllers\UsersController::class, 'index'])->name('profileupdate.api');


        Route::group(['prefix'=> 'admin', 'middleware'=> ['api.superAdmin']], function(){
            Route::group(['prefix' => 'users'], function () {
               Route::get('all', [\App\Http\Controllers\UsersController::class, 'getAllUsers']);
               Route::post('{user_id}/ban', [\App\Http\Controllers\UsersController::class, 'banUser']);
               Route::post('{user_id}/unban', [\App\Http\Controllers\UsersController::class, 'unbanUser']);
            });
            //Manage Drug Companies can be access by user type admin super admin
            Route::group(['prefix' => 'companies'], function (){
                Route::get('/all', [\App\Http\Controllers\DrugCompaniesController::class, 'index']);
                Route::post('/save', [\App\Http\Controllers\DrugCompaniesController::class, 'save']);
                Route::post('/{company_id}', [\App\Http\Controllers\DrugCompaniesController::class, 'delete']);
            });
            // Manage Drugs by Administrators
            Route::group(['prefix'=>'drugs'], function(){
                Route::get('/all', [\App\Http\Controllers\DrugsController::class, 'index']);
                Route::post('/save', [\App\Http\Controllers\DrugsController::class, 'save']);
                Route::post('/{drug_id}', [\App\Http\Controllers\DrugsController::class, 'delete']);
            });
            // Manage Users List by administrator
        });

        Route::group(['prefix'=>'covid-test'], function(){
            //Perform Test-> upload image and hit python API end point
            Route::post('/get-result',[\App\Http\Controllers\CovidTestsController::class, 'index'])->name('performtests.api');
            Route::post('/save',[\App\Http\Controllers\CovidTestsController::class, 'save'])->name('saveTest.api');
        });

        Route::group(['prefix'=>'history'], function(){
            //Tests History ->
            Route::get('/covid-test', [\App\Http\Controllers\HistoriesController::class, 'covidTests']);
            //Route::get('/medication',  [\App\Http\Controllers\HistoriesController::class, 'medicationHistory']);
            //Test History By doctor
            Route::get('/{patient_id}/covid-test', [\App\Http\Controllers\HistoriesController::class, 'getPatientTests']);
            //Patient's medical history
            //Route::get('/{patient_id}/medication', [\App\Http\Controllers\HistoriesController::class, 'getMedicationHistory']);
        });
        Route::group(['prefix'=>'patient'], function(){
            //Get Patients lists by doctor
            Route::get('/all', [\App\Http\Controllers\PatientsController::class, 'index']);
            Route::get('/search', [\App\Http\Controllers\PatientsController::class, 'search']);
            Route::get('/{patientId}', [\App\Http\Controllers\PatientsController::class, 'getPatient']);
            Route::get('/{patientId}/user', [\App\Http\Controllers\PatientsController::class, 'getPatientUser']);
            Route::post('/', [\App\Http\Controllers\PatientsController::class, 'savePatient']);
            Route::post('/{patientId}', [\App\Http\Controllers\PatientsController::class, 'removePatient']);
            //Medication of patients
            Route::get('/{patient_id}/medication', [\App\Http\Controllers\PatientsController::class, 'medication']); // current medication
            Route::get('/{patient_id}/medication/{medication_id}', [\App\Http\Controllers\PatientsController::class, 'getMedicationDetail']); // current medication
            Route::post('/{patient_id}/medication', [\App\Http\Controllers\PatientsController::class, 'saveMedication']); // save medication
        });
        Route::group(['prefix'=>'drugs'], function(){
            Route::get('/search', [\App\Http\Controllers\DrugsController::class, 'search']);
        });


        //test
        Route::get('/articles', [\App\Http\Controllers\ArticleController::class, 'index'])->name('articles');
    });
});

